package com.jestermen.grandPrixGranny.characters;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;

/**
 * This is an abstract base class for the different types of characters in Grand Prix Granny.
 * 
 * @author Adam Testerman
 */
public abstract class Character 
{
	//PRIMITIVES
	protected float _dx;
	protected float _dy;
	
	//OBJECTS
	protected Sprite _view;
	protected Rectangle _bounds;
	
	
	//=================================
	//		SETUP
	//=================================
	
	/**
	 * Constructor. Creates a new instance of <code>Character</code>.
	 */
	public Character()
	{
		_dx = 0;
		_dy = 0;
		
		_view = new Sprite(new Texture("npc1.png"));
		
		_bounds = new Rectangle();
		_bounds.set(0, 0, _view.getWidth(), _view.getHeight());
	}
	
	
	//=================================
	//		MISC.
	//=================================
	
	/**
	 * Moves this character's bounds to the specified coordinates, then centers the view on the bounds.
	 */
	public void setPosition(float x, float y)
	{
		_bounds.setX(x);
		_bounds.setY(y);
		
		_view.setPosition(_bounds.x + _bounds.width / 2 - _view.getWidth() / 2, _bounds.y + _bounds.height / 2 - _view.getHeight() / 2);
	}
	
	/**
	 * Sets the texture of this character's view to the image at the specified path. The current texture will be 
	 * disposed first.
	 */
	public void setTexture(String path)
	{
		_view.getTexture().dispose();
		_view.setTexture(new Texture(path));
	}
	
	
	//=================================
	//		ACCESSORS
	//=================================
	
	public Rectangle getBounds()
	{
		return _bounds;
	}
	
	protected void setBounds(Rectangle value)
	{
		_bounds = value;
	}
	
	public float getDX()
	{
		return _dx;
	}
	
	public void setDX(float value)
	{
		_dx = value;
	}
	
	public Sprite getView()
	{
		return _view;
	}
}