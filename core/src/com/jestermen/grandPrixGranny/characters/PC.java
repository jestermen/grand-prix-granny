package com.jestermen.grandPrixGranny.characters;

import java.util.Random;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.jestermen.grandPrixGranny.GrandPrixGranny;

/**
 * This class represents the PC (the granny) in Grand Prix Granny.
 * 
 * @author Adam Testerman
 */
public class PC extends Character 
{
	//CONSTANTS
	private final int BOUNDS_WIDTH = 			55;
	private final int BOUNDS_HEIGHT = 			83;
	private final int START_Y = 				-100;
	private final int END_Y = 					32;
	private final int MAX_DX = 					8;		//pixels per frame
	private final float ACCELERATION_X = 		.5f;
	private final float ACCELERATION_Y =		2.5f;
	private final int DEAD_START_Y = 			-500;
	private final float DEAD_START_DY_MIN =		11f;
	private final float DEAD_START_DY_MAX =		12f;
	private final float DEAD_DECAY =			.05f;	//how quickly the dead PC decelerates (percent per frame)
	
	//PRIMITIVES
	private float _deadDX;
	private float _deadDY;
	private boolean _isVeering;
	
	//OBJECTS
	private Sprite _viewDead;
	
	
	//=================================
	//		SETUP
	//=================================
	
	/**
	 * Constructor. Creates a new instance of <code>PC</code>.
	 */
	public PC()
	{
		_dx = 0;
		_dy = 0;
		
		_view = new Sprite(new Texture("pc.png"));
		
		_viewDead = new Sprite(new Texture("pc_dead.png"));
		_viewDead.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);	//anti-alias
		
		_bounds = new Rectangle();
		_bounds.set(0, 0, BOUNDS_WIDTH, BOUNDS_HEIGHT);
		
		_isVeering = false;
	}
	

	//=================================
	//		MISC.
	//=================================
	
	/**
	 * Updates this object on a per-frame basis.
	 */
	public void update(Vector3 touchPosition)
	{
		//UPDATE DX
		//If the player is touching the screen, accelerate PC toward the touch
		if (touchPosition != null)
		{
			//Stop veering if needed
			if (_isVeering)
			{
				_isVeering = false;
				_dx = 0;	//so the player doesn't have to fight against the inertia
			}
			
			//If player is pressing on left side of screen, accelerate left
			if (touchPosition.x < GrandPrixGranny.SCENE_WIDTH / 2)
			{
				_dx -= ACCELERATION_X;
			}
			//Otherwise, accelerate right
			else if (touchPosition.x > GrandPrixGranny.SCENE_WIDTH / 2)
			{
				_dx += ACCELERATION_X;
			}
		}
		//Otherwise, as long as the PC isn't veering, decelerate
		else if (!_isVeering)
		{
			if (_dx < 0)
			{
				_dx += ACCELERATION_X;
				
				if (_dx > 0) _dx = 0;
			}
			else if (_dx > 0)
			{
				_dx -= ACCELERATION_X;
				
				if (_dx < 0) _dx = 0;
			}
		}
		
		//Bounds check dx
		if (_dx < -MAX_DX)
		{
			_dx = -MAX_DX;
		}
		else if (_dx > MAX_DX)
		{
			_dx = MAX_DX;
		}
		//Start veering if stopped
		else if (_dx == 0)
		{
			_isVeering = true;
			
			//Randomly make the PC veer left or right
			if (new Random().nextInt(2) == 0)
			{
				_dx = -ACCELERATION_X;
			}
			else
			{
				_dx = ACCELERATION_X;
			}
		}
		
		//UPDATE DY
		//If PC has reached the ending y-coordinate, no need to move it
		if (_bounds.y == END_Y)
		{
			_dy = 0;
		}
		//Otherwise, keep moving PC toward it
		else
		{
			_dy = ACCELERATION_Y;
		}
		
		//UPDATE VIEW
		setPosition(_bounds.getX() + _dx, _bounds.getY() + _dy);
		
		//BOUNDS CHECK Y
		if (_bounds.y > END_Y)
		{
			setPosition(_bounds.x, END_Y);
		}
	}
	
	/**
	 * Updates this object's dead body.
	 */
	public void updateDead()
	{
		_viewDead.setPosition(_viewDead.getX() + _deadDX, _viewDead.getY() + _deadDY);
		
		//Decelerate
		_deadDX *= (1 - DEAD_DECAY);
		_deadDY *= (1 - DEAD_DECAY);
		
		if (_deadDX < 1 && _deadDY < 1) 
		{
			_deadDX = 0;
			_deadDY = 0;
		}
		else
		{
			_viewDead.rotate(_deadDY);
		}
	}
	
	/**
	 * Sets this object to its starting parameters.
	 */
	public void reset()
	{
		_dx = 0;
		_dy = 0;
		
		setTexture("pc.png");
		setPosition(GrandPrixGranny.SCENE_WIDTH / 2 - getBounds().width / 2, START_Y);
		
		_viewDead.setPosition(_viewDead.getX(), DEAD_START_Y);
	}
	
	/**
	 * Sets this object's dead body to its starting parameters.
	 */
	public void resetDead()
	{
		//Center dead PC on PC
		int pcCenterX = (int)getBounds().x + (int)getBounds().width / 2;
		int pcCenterY = (int)getBounds().y + (int)getBounds().height / 2;
		_viewDead.setPosition(pcCenterX - _viewDead.getWidth() / 2, pcCenterY - _viewDead.getHeight() / 2);
		
		//Randomly determine how quickly the dead PC will start flying forward
		Random random = new Random();
		float randDY = random.nextFloat() * (DEAD_START_DY_MAX - DEAD_START_DY_MIN) + DEAD_START_DY_MIN;
		
		_deadDX = _dx;
		_deadDY = randDY;
		
		//Randomly set start rotation
		float randAngle = random.nextFloat() * 365;
		
		_viewDead.setRotation(randAngle);
	}
	
	
	//=================================
	//		ACCESSORS
	//=================================
	
	public float getDeadDX()
	{
		return _deadDX;
	}
	
	public float getDeadDY()
	{
		return _deadDY;
	}
	
	public Sprite getViewDead()
	{
		return _viewDead;
	}
}