package com.jestermen.grandPrixGranny.characters;

import java.util.Random;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;

/**
 * This class represents the NPCs (the oncoming cars) in Grand Prix Granny.
 * 
 * @author Adam Testerman
 */
public class NPC extends Character 
{
	//CONSTANTS
	private final int BOUNDS_WIDTH = 			69;
	private final int BOUNDS_HEIGHT = 			95;
	private final float MIN_DY = 				-4.5f;
	private final float MAX_DY = 				-3.5f;
	private final float ACCELERATION_Y = 		.01f;
	
	//PRIMITIVES
	private boolean isAccelerating;
	
	
	//=================================
	//		SETUP
	//=================================
	
	/**
	 * Constructor. Creates a new instance of <code>NPC</code>.
	 */
	public NPC()
	{
		_dx = 0;
		randomizeDY();
		
		isAccelerating = (new Random().nextInt(2) == 0 ? true : false);
		
		_view = new Sprite(new Texture("npc1.png"));
		randomizeTexture();
		
		_bounds = new Rectangle();
		_bounds.set(0, 0, BOUNDS_WIDTH, BOUNDS_HEIGHT);
	}
	
	
	//=================================
	//		MISC.
	//=================================
	
	/**
	 * Updates this object on a per-frame basis.
	 */
	public void update()
	{
		//UPDATE DY
		if (isAccelerating)
		{
			_dy -= ACCELERATION_Y;
		}
		else
		{
			_dy += ACCELERATION_Y;
		}
		
		//Bounds check dy
		if (_dy > MAX_DY)
		{
			_dy = MAX_DY;
			
			isAccelerating = true;
		}
		else if (_dy < MIN_DY)
		{
			_dy = MIN_DY;
			
			isAccelerating = false;
		}
		
		//UPDATE VIEW
		setPosition(getBounds().x + _dx, getBounds().y + _dy);
	}
	
	/**
	 * Chooses a random texture for this object's view amongst predetermined images.
	 */
	public void randomizeTexture()
	{
		int rand = new Random().nextInt(3);
		if (rand == 0)
		{
			setTexture("npc1.png");
		}
		else if (rand == 1)
		{
			setTexture("npc2.png");
		}
		else 
		{
			setTexture("npc3.png");
		}
	}
	
	/**
	 * Chooses a random dy for this object within certain bounds.
	 */
	public void randomizeDY()
	{
		_dy = new Random().nextFloat() * (MAX_DY - MIN_DY) + MIN_DY;
	}
}