package com.jestermen.grandPrixGranny.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class MuteButton extends Actor 
{
	TextureRegion _region;
	Texture _soundOn;
	Texture _soundOff;
	
    public MuteButton() 
    {
    	_soundOn = new Texture("sound_on.png");
    	_soundOn.setFilter(TextureFilter.Linear, TextureFilter.Linear);	//anti-alias
    	_soundOff = new Texture("sound_off.png");
    	_soundOff.setFilter(TextureFilter.Linear, TextureFilter.Linear);
    	
    	_region = new TextureRegion(_soundOn);
    	
    	setOn();
    	
    	setBounds(0, 0, _soundOn.getWidth(), _soundOn.getHeight());
    	setOrigin(_soundOn.getWidth() / 2, _soundOn.getHeight() / 2);
    }
    
    public void setOn()
    {
    	_region.setTexture(_soundOn);
    }
    
    public void setOff()
    {
    	_region.setTexture(_soundOff);
    }
    
    @Override
    public void draw (Batch batch, float parentAlpha) 
    {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        batch.draw(_region, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
    }
}