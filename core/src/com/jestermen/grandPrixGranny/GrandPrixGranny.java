package com.jestermen.grandPrixGranny;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.jestermen.grandPrixGranny.characters.Character;
import com.jestermen.grandPrixGranny.characters.NPC;
import com.jestermen.grandPrixGranny.characters.PC;
import com.jestermen.grandPrixGranny.interfaces.IActivityRequestHandler;
import com.jestermen.grandPrixGranny.ui.MuteButton;

import java.util.Random;

public class GrandPrixGranny extends ApplicationAdapter 
{
	//CONSTANTS
	public static final int SCENE_WIDTH	=			800;
	public static final int SCENE_HEIGHT = 			450;
	private final int NUM_NPCS_PER_ROW = 			5;
	private final int NPC_ROW_MARGIN = 				430;	//number of pixels between each row of NPCs
	private final int NUM_NPC_ROWS =				2;
	private final float BG_SPEED = 					2.5f;
	private final int TRACK_LEFT = 					120;	//far left side of the actual race track on the background
	private final int TRACK_RIGHT = 				SCENE_WIDTH - 120;
	private final int TRACK_WIDTH = 				TRACK_RIGHT - TRACK_LEFT;
	private final int GAME_OVER = 					0;		//the "tap to start" "screen"
	private final int GAME_PLAYING = 				1;
	
	//PRIMITIVES
	int _gameState;
	int _currentEmptySlotIndex;
	int _currentScore;
	int _highScore;
	int _currentNPCRowIndex;
	boolean _isRestartable;
	boolean _isSoundMuted;
	boolean _isAndroid;
	boolean _isiOS;
	boolean _isMuteButtonDown;
	
	//OBJECTS
	SpriteBatch _batch;
	OrthographicCamera _camera;
	PC _pc;
	Sprite _bg1;
	Sprite _bg2;
	Vector3 _touchPosition;							//class-level so we don't have to create a new one each update
	Stage _stage;
	Viewport _viewport;
	BitmapFont _bmpFont;
	MuteButton _muteButton;
	IActivityRequestHandler _handler;
	Sound _crashSound;
	Music _music;
	
	//LISTS
	NPC[][] _npcs;
	
	public GrandPrixGranny(IActivityRequestHandler IARH)
	{
		_handler = IARH;
	}
	
	@Override
	public void create()
	{
		switch (Gdx.app.getType()) 
		{
			case Android:
				_isAndroid = true;
				break;
			case Desktop:
				break;
			case WebGL:
				break;
			case iOS:
				_isiOS = true;
				break;
			default:
		}
		
		_gameState = GAME_OVER;
		
		_highScore = loadHighScore();
		
		_batch = new SpriteBatch();
		
		_muteButton = new MuteButton();
		_muteButton.addListener(new InputListener() {
			@Override
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) 
			{
				_muteButton.setScale(.8f);
				
				return true;
			}
			
			@Override
			public void touchUp (InputEvent event, float x, float y, int pointer, int button) 
			{
				_muteButton.setScale(1f);
				
				_isSoundMuted = !_isSoundMuted;
				
				if (_isSoundMuted)
				{
					_muteButton.setOff();
					
					_music.setVolume(0);
				}
				else
				{
					_muteButton.setOn();
					
					_music.setVolume(1);
				}
			}
		});
		
		_camera = new OrthographicCamera();
		_camera.position.set(SCENE_WIDTH / 2, SCENE_HEIGHT / 2, 0);
		_viewport = new FillViewport(SCENE_WIDTH, SCENE_HEIGHT, _camera);
		_stage = new Stage(_viewport);
		_stage.addActor(_muteButton);
		
		Gdx.input.setInputProcessor(_stage);
		
		_bmpFont = new BitmapFont(Gdx.files.internal("font2-export.txt"), Gdx.files.internal("font2-export.png"), false);
		//bmpFont.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);	//supposed to help with antialiasing but doesn't seem to work
		
		_bg1 = new Sprite(new Texture("bg.jpg"));
		_bg2 = new Sprite(new Texture("bg.jpg"));
		
		_pc = new PC();
		
		_npcs = new NPC[NUM_NPC_ROWS][NUM_NPCS_PER_ROW];
		for (int i = 0; i < _npcs.length; i++)
		{
			NPC row[] = new NPC[NUM_NPCS_PER_ROW];
			for (int j = 0; j < row.length; j++)
			{
				row[j] = new NPC();
			}
			
			_npcs[i] = row;
		}
		
		_crashSound = Gdx.audio.newSound(Gdx.files.internal("car_crash.wav"));
		_music = Gdx.audio.newMusic(Gdx.files.internal("music.mp3"));
		_music.setLooping(true);
		
		_touchPosition = new Vector3();
		
		reset();
	}
	
	@Override
	public void resize(int width, int height)
	{
		_viewport.update(width, height, true);
		
		Vector2 screenTopRight = _viewport.unproject(new Vector2(Gdx.graphics.getWidth(), 0));
		_muteButton.setPosition(screenTopRight.x - _muteButton.getWidth() - 10, screenTopRight.y - _muteButton.getHeight() - 10);
	}

	@Override
	public void render()
	{
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		updateScene();
		drawScene();
		_stage.act();
		_stage.draw();

		//System.out.println("Gutter widths (L/R/T/B): " + viewport.getLeftGutterWidth() + ", " + viewport.getRightGutterWidth() + ", " + viewport.getTopGutterHeight() + ", " + viewport.getBottomGutterHeight());
		//System.out.println("Right gutter X, top gutter Y: " + viewport.getRightGutterX() + ", " + viewport.getTopGutterY());
		//System.out.println("Screen (X, Y, W, H): " + viewport.getScreenX() + ", " + viewport.getScreenY() + ", " + viewport.getScreenWidth() + ", " + viewport.getScreenHeight());
		//System.out.println("World (W, H): " + viewport.getWorldWidth() + ", " + viewport.getWorldHeight());
	}
	
	@Override
	public void dispose()
	{
		_batch.dispose();
		_bmpFont.dispose();
		_crashSound.dispose();
	}
	
	private void updateScene()
	{
		if (_gameState == GAME_OVER)
		{
			if (Gdx.input.justTouched() && _isRestartable)
			{
				reset();
				
				_gameState = GAME_PLAYING;
				
				if (!_isSoundMuted)
				{
					_music.play();
				}
			}
			else
			{
				_pc.updateDead();
				
				if (_pc.getDeadDX() == 0 && _pc.getDeadDY() == 0) _isRestartable = true;
			}
		}
		else if (_gameState == GAME_PLAYING)
		{
			updateBackground();
			updatePC();
			updateNPCs();
			checkCollisions();
		}
	}
	
	private void updateBackground()
	{
		//Update background positions
		_bg1.setPosition(_bg1.getX(), _bg1.getY() - BG_SPEED);
		if (_bg1.getY() == -(_bg1.getHeight()))
		{
			_bg1.setPosition(_bg1.getX(), SCENE_HEIGHT);
		}
		
		_bg2.setPosition(_bg2.getX(), _bg2.getY() - BG_SPEED);
		if (_bg2.getY() == -(_bg2.getHeight()))
		{
			_bg2.setPosition(_bg2.getX(), SCENE_HEIGHT);
		}
	}

	private void updatePC()
	{
		if (Gdx.input.isTouched())
		{
			_touchPosition.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			_camera.unproject(_touchPosition);
			
			_pc.update(_touchPosition);
		}
		else
		{
			_pc.update(null);
		}
	}
	
	private void updateNPCs()
	{
		for (int i = 0; i < _npcs.length; i++)
		{
			for (int j = 0; j < _npcs[i].length; j++)
			{
				_npcs[i][j].update();
			}
		}
	}
	
	private void drawScene()
	{
		_camera.update();
		_batch.setProjectionMatrix(_camera.combined);
		_batch.begin();
		_batch.disableBlending();
		
		_bg1.draw(_batch);
		_bg2.draw(_batch);
		_batch.enableBlending();
		
		_pc.getView().draw(_batch);
		
		for (int i = 0; i < _npcs.length; i++)
		{
			for (int j = 0; j < _npcs[i].length; j++)
			{
				_npcs[i][j].getView().draw(_batch);
			}
		}
		
		Vector2 screenTopLeft = _viewport.unproject(new Vector2(0, 0));
		
		_muteButton.draw(_batch, 1);
		
		_bmpFont.draw(_batch, Integer.toString(_currentScore), screenTopLeft.x + 10, SCENE_HEIGHT + 45);
		
		if 		(_gameState == GAME_OVER)
		{
			_pc.getViewDead().draw(_batch);
			
			CharSequence high = "High Score: " + Integer.toString(_highScore);
			_bmpFont.draw(_batch, high, SCENE_WIDTH / 2 - _bmpFont.getBounds(high).width / 2, SCENE_HEIGHT / 2 - 100);
			
			if (_isRestartable)
			{
				CharSequence startPrompt = "Tap to Start";
				_bmpFont.draw(_batch, startPrompt, SCENE_WIDTH / 2 - _bmpFont.getBounds(startPrompt).width / 2, SCENE_HEIGHT / 2 + _bmpFont.getLineHeight());
			}
		}
		
		_batch.end();
	}
	
	private void checkCollisions()
	{
		//PC VS. WALLS
		float pcLeft = _pc.getBounds().x;
		float pcRight = _pc.getBounds().x + _pc.getBounds().width;
		if (pcLeft < TRACK_LEFT || pcRight > TRACK_RIGHT)
		{
			float bounceAmount = .9f;
			_pc.setPosition(_pc.getBounds().x - _pc.getDX(), _pc.getBounds().y);		//put PC back where it was
			_pc.setDX(-_pc.getDX() * bounceAmount);				//then bounce a little
		}
		
		//PC VS. NPCS
		NPC thisNPC;
		for (int i = 0; i < _npcs.length; i++)
		{
			for (int j = 0; j < _npcs[i].length; j++)
			{
				thisNPC = _npcs[i][j];
				
				if (isColliding(_pc, thisNPC))
				{
					gameOver();
					
					break;
				}
			}
		}
		
		//NPCS VS. LEVEL
		for (int i = 0; i < _npcs.length; i++)
		{
			for (int j = 0; j < _npcs[i].length; j++)
			{
				thisNPC = _npcs[i][j];
				
				if (thisNPC.getBounds().y < -(thisNPC.getBounds().height) - 30)	//-30 to account for y variations within row
				{
					int resetY;
					if (i == 0)
					{
						resetY = (int)_npcs[_npcs.length - 1][0].getBounds().y + NPC_ROW_MARGIN;
					}
					else
					{
						resetY = (int)_npcs[i - 1][0].getBounds().y + NPC_ROW_MARGIN;
					}
					
					resetNPCRow(_npcs[i], resetY);
					
					_currentScore++;
				}
			}
		}
	}
	
	private boolean isColliding(Character char1, Character char2)
	{
		float char1Left = char1.getBounds().x;
		float char1Right = char1.getBounds().x + char1.getBounds().width;
		float char1Top = char1.getBounds().y + char1.getBounds().height;
		float char1Bottom = char1.getBounds().y;
		
		float char2Left = char2.getBounds().x;
		float char2Right = char2.getBounds().x + char2.getBounds().width;
		float char2Top = char2.getBounds().y + char2.getBounds().height;
		float char2Bottom = char2.getBounds().y;
		
	    if (char1Left > char2Right || char1Right < char2Left)
	        return false;
	 
	    if (char1Top < char2Bottom || char1Bottom > char2Top)
	        return false;
	 
	    return true;
	}
	
	private void resetNPCRow(NPC[] row, int yCoord)
	{
		//Randomize the order of the cars in this row
		int index;
		NPC temp;
	    Random random = new Random();
	    for (int i = row.length - 1; i > 0; i--)
	    {
	        index = random.nextInt(i + 1);
	        temp = row[index];
	        row[index] = row[i];
	        row[i] = temp;
	    }
		
	    //Randomly place an empty slot amidst the cars in this row
		int numSlots = NUM_NPCS_PER_ROW + 1;	//+1 to account for the empty slot
		int emptySlotIndex;
		do
		{
			emptySlotIndex = new Random().nextInt(numSlots);
		}
		while (emptySlotIndex == _currentEmptySlotIndex);
		_currentEmptySlotIndex = emptySlotIndex;
		
		NPC thisNPC;
		for (int i = 0; i < numSlots; i++)
		{
			if (i == emptySlotIndex) 
			{
				continue;
			}
			else
			{
				if (i > emptySlotIndex)
				{
					thisNPC = row[i - 1];
				}
				else
				{
					thisNPC = row[i];
				}
				
				//X-coordinate: Evenly distribute cars along the track
				int marginWidth = (TRACK_WIDTH - (numSlots * (int)thisNPC.getBounds().width)) / (numSlots + 1);
				int segmentWidth = marginWidth + (int)thisNPC.getBounds().width;
				int thisNPCX = TRACK_LEFT + segmentWidth * i + marginWidth;
				
				thisNPC.randomizeDY();
				thisNPC.randomizeTexture();
				thisNPC.setPosition(thisNPCX, yCoord);
			}
		}
	}

	private int loadHighScore()
	{
		FileHandle highScoresFile = Gdx.files.local("highScore");
		
		if (highScoresFile.exists())
		{
			return Integer.valueOf(highScoresFile.readString());
		}
		else
		{
			return 0;
		}
	}
	
	private void saveHighScore()
	{
		FileHandle highScoresFile = Gdx.files.local("highScore");
		
		highScoresFile.writeString(Integer.toString(_currentScore), false);
	}
	
	private void gameOver()
	{
		_gameState = GAME_OVER;
		
		if (!_isSoundMuted)
		{
			_crashSound.play();
		}
		_music.stop();
		
		_isRestartable = false;
		
		if (_currentScore > _highScore)
		{
			_highScore = _currentScore;
			
			saveHighScore();
		}
		
		_pc.resetDead();
		
		_pc.setTexture("pc_car.png");	//empty car
		
		if (_isAndroid)
		{
			_handler.showAds(true);
		}
	}
	
	private void reset()
	{
		_currentScore = 0;

		_currentNPCRowIndex = 0;
		
		_bg1.setPosition(0, 0);
		_bg2.setPosition(0, SCENE_HEIGHT);
		
		_pc.reset();
		
		for (int i = 0; i < _npcs.length; i++)
		{
			resetNPCRow(_npcs[i], SCENE_HEIGHT + NPC_ROW_MARGIN * i);	//+50 just to make sure they're off screen
		}
		
		if (_isAndroid)
		{
			_handler.showAds(false);
		}
	}
}